from turtle import Turtle

FONT = ('Courier', 32, 'normal')
ALIGN = 'center'
POSITION = ((-100, 220), (100, 220))

class Score(Turtle):

    def __init__(self, posX,posY):
        super().__init__()
        self.goto(posX,posY)
        self.color('white')
        self.penup()
        self.hideturtle()


    def update(self, count):
        self.write(f'Zostało:{count}',align=ALIGN, font=FONT)

    def game_over(self, wl):
            self.clear()
            self.goto(0,0)
            if wl:
                self.write(f'🥇 Wygrana!!! 🥇', align=ALIGN, font=FONT)
            else:
                self.write(f'😵‍💫 Przegrales... 😵‍💫', align=ALIGN, font=FONT)

