from turtle import Turtle
# from main import screen
START = -320
X = 55
class Paddle(Turtle):

    def __init__(self):
        super().__init__()
        self.create_paddle()

    def create_paddle(self):
        self.shape('square')
        self.color('white')
        self.penup()
        self.goto(0, START)

        self.turtlesize(1,8)

    def Left(self):
        x = self.xcor() - X
        self.goto(x,self.ycor())

    def Right(self):
        x = self.xcor() + X
        self.goto(x, self.ycor())

