from turtle import Screen
from paddle import Paddle
from ball import Ball
from blocks import Blocks
from score import Score

BOUNCE_X = 30
BOUNCE_Y = 50

# Screen:
screen = Screen()
screen.setup(width=860, height=720, starty=350)
screen.bgcolor('black')
screen.title('Breakout Game')
screen.tracer(0)

# Paddle:
pd = Paddle()

# Listen
screen.listen()

screen.onkey(pd.Left, 'a')
screen.onkey(pd.Right, 'd')

# Ball:
ball = Ball()

# Score:
score = Score((-screen.window_width() // 3), (screen.window_height() // 2) - 40)

# Bricks:
WIDTH = screen.window_width() // 2
HEIGHT = screen.window_height() // 2

w_b = -WIDTH + 70
w_h = HEIGHT - 60

bricks = []

for x in range(7):
    for y in range(9):
        brick: Blocks = Blocks((w_b, w_h))
        bricks.append(brick)
        w_b += 87
    w_b = -WIDTH + 70
    w_h -= 28

##

game_on = True
while game_on:

    screen.update()

    ball.move()

    score.update(len(bricks))
    # Test if :
    if ball.ycor() > HEIGHT - BOUNCE_Y:
        ball.bounce_y()
    # Width bounce:

    if ball.xcor() > WIDTH - BOUNCE_X or ball.xcor() < -WIDTH + BOUNCE_X:
        ball.bounce_x()
    # Paddle bounce:
    if ball.distance(pd) < 80 and ball.ycor() <= -300:
        ball.bounce_y()

    # Collision:
    for brick in bricks:
        if ball.distance(brick) < 30:
            ball.bounce_y()
            brick.hideturtle()

            bricks.remove(brick)
            score.clear()
    # Win
    if len(bricks) == 0:
        score.game_over(True)
        game_on = False

    # Lose:
    if ball.ycor() <= -400:
        score.game_over(False)
        game_on = False

screen.exitonclick()
