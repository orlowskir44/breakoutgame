from turtle import Turtle

MOVE = 5
MOVE_SPEED = 0.01


class Ball(Turtle):

    def __init__(self):
        super().__init__()

        self.shape('circle')
        self.color('aqua')
        self.penup()
        self.x_move = MOVE
        self.y_move = MOVE
        self.move_speed = MOVE_SPEED

    def move(self):
        x = self.xcor() + self.x_move
        y = self.ycor() + self.y_move
        self.goto(x, y)

    def bounce_y(self):
        self.y_move *= -1

    def bounce_x(self):
        self.x_move *= -1

    def m_speed(self):
        self.move_speed += 0.9
