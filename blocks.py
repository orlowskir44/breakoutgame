from turtle import Turtle
import random

COLORS = ['red','blue','green','yellow','brown','white']

class Blocks(Turtle):

    def __init__(self, position):
        super().__init__()
        self.shape('square')
        self.color(random.choice(COLORS))
        self.shapesize(stretch_len=4, stretch_wid=0.95)
        self.penup()
        self.goto(position)